// HOME -> jogos acontecendo
$(document).ready(function(){
	$('.carousel-jogos-acontecendo').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		// variableWidth: true,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-up.png" class="btn-slick-carousel-up" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-down.png" class="btn-slick-carousel-down" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 769,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
			}
		}
		]
	});
});

// HOME -> ultimos jogos
$(document).ready(function(){
	$('.carousel-ultimos-jogos').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		centerMode: true,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1661,
			settings: {
				slidesToShow: 5,
			}
		}, {
			breakpoint: 1441,
			settings: {
				slidesToShow: 4,
			}
		}, {
			breakpoint: 1025,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 769,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
				centerMode: false,
			}
		}, {
			breakpoint: 376,
			settings: {
				slidesToShow: 1,
				centerMode: false,
			}
		}
		]
	});
});

// HOME -> ultimos jogos
$(document).ready(function(){
	$('.carousel-ligas').slick({
		infinite: true,
		slidesToShow: 6,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		// centerMode: true,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1441,
			settings: {
				slidesToShow: 5,
			}
		},{
			breakpoint: 1025,
			settings: {
				slidesToShow: 5,
			}
		}, {
			breakpoint: 769,
			settings: {
				slidesToShow: 4,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 376,
			settings: {
				slidesToShow: 2,
				centerMode: false,
			}
		}
		]
	});
});

// HOME -> Como funciona
$(document).ready(function(){
	$('.carousel-pista').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		autoplay: false,
		autoplaySpeed: 5000,
		fade: true,
		asNavFor: '.slider-nav'
	});

	$('.slider-nav').slick({
		slidesToShow: 3,
	  	slidesToScroll: 1,
	  	asNavFor: '.carousel-pista',
	  	dots: false,
	  	arrows: true,
	  	prevArrow:'<img src="imagens/logos/arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
	  	centerMode: true,
	  	variableWidth: true,
	  	focusOnSelect: true,
	  	responsive: [
	  	{
	  		breakpoint: 480,
	  		settings: {
	  			slidesToShow: 1,
	  		}
	  	}
	  	]
	});
});

//Iniciar animações
AOS.init({
	disable: 'mobile',
	delay: 0,
	duration: 1200,
	once: true,
});